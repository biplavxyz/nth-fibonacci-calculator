use std::io;

fn main() {
    let nth: f64;
    //Input validation 
    loop {
        println!("Please enter the nth number of which you want to find fibonacci value: ");
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read input.");

        let input: f64 = match input.trim().parse::<f64>() {
            Ok(parsed_input) => parsed_input,
            Err(_) => continue,
        };
        nth = input;
        break;
    }
    
    //Binet's Formula
    let nth_number: f64;
    nth_number = (((1.0+5.0_f64.sqrt())/2.0).powf(nth)-((1.0-5.0_f64.sqrt())/2.0).powf(nth))/5.0_f64.sqrt();

    println!("The nth number {} has fibonacci value {:.0}", nth, nth_number);

}
