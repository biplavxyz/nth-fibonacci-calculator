# Nth Fibonacci Calculator

This program calculates nth Fibonacci number.
Needs to have rust installed in the system.

Compile the code as
```rust
rustc fib.rs 
```
Then, run the compiled binary as 
```rust
./fib
```
Provide the nth number of which you want to calculate Fibonacci value, and the program will print out the nth Fibonacci number.  

Code is written without using any recursion.
The concept of Binet's Fibonacci Number formula is used to calculate nth Fibonacci number.
